#!/usr/bin/env bash

set -eux

idea_version=2019.3.5
idea_build=193.7288.26
export DEBIAN_FRONTEND=noninteractive

test "$(whoami)" = root

cd /tmp

ls -lRa /var/tmp
chmod a+rx /var/tmp

mkdir -p /home/course/.ssh
cp /var/tmp/id_rsa /home/course/.ssh/
chown -R course.course /home/course/.ssh
chmod 600 /home/course/.ssh/id_rsa

apt-get update
apt-get install --yes apt-utils
apt-get upgrade --yes
apt-get install --yes sudo wget
echo "%sudo ALL=NOPASSWD: ALL" > /etc/sudoers.d/nopasswd

apt-get install --yes gnupg
# Podman installation instructions
. /etc/os-release
sh -c "echo 'deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
wget -q -O - https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/Release.key | apt-key add -

# Chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google-chrome.list
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome-remote-desktop/deb/ stable main' > /etc/apt/sources.list.d/chrome-remote-desktop.list

apt-get update
apt-get --yes install podman

apt-get install --yes docker.io
usermod -a -G docker course

apt-get install --yes git openjdk-11-jdk python3 python3-pip
apt-get install --yes kubuntu-desktop sddm kde-config-sddm

apt-get install --yes google-chrome-stable
apt-get install --yes chrome-remote-desktop

if ! [[ -d /usr/local/idea-IC-$idea_build ]]; then
  wget --quiet https://download.jetbrains.com/idea/ideaIC-${idea_version}.tar.gz

  test "$(sha256sum /tmp/ideaIC-${idea_version}.tar.gz | awk '{print $1}')" = \
    46a6fffd016d6aa32b71c0fcfea063effad3ffb57f3146046911ad7d1bfffdc2

  tar -C /usr/local -xf /tmp/ideaIC-${idea_version}.tar.gz
  rm ideaIC-${idea_version}.tar.gz
fi
ln -sf /usr/local/idea-IC-${idea_build}/bin/idea.sh /usr/local/bin/idea.sh
