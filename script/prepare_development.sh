#!/usr/bin/env bash

set -eux

repo=data-eng-course
scala_plugin_version=2019.3.27

test "$(whoami)" = course

export HOME=/home/course
cd $HOME

tar xf /var/tmp/user_setup.tar.gz

mkdir -p .config/plasma-workspace/env
mv start_idea.sh .config/plasma-workspace/env/
chmod +x .config/plasma-workspace/env/start_idea.sh

mkdir -p .config
mv kde/kxkbrc kde/plasma-org.kde.plasma.desktop-appletsrc .config/

mkdir -p Desktop
mv kde/*.desktop Desktop

cd /tmp
wget --quiet  \
  https://plugins.jetbrains.com/files/1347/82127/scala-intellij-bin-${scala_plugin_version}.zip
test "$(sha256sum scala-intellij-bin-${scala_plugin_version}.zip | awk '{print $1}')" = \
  55b63dceae72a2e420e374045c0b615624eaf8e48074757e5cd348ae1822852b

wget --quiet https://plugins.jetbrains.com/files/7322/85764/python-ce.zip
test "$(sha256sum python-ce.zip | awk '{print $1}')" = \
  718750acf836c9e9c85fe4d342a619afb26932bc0d77e7adfa25be1facb8a23f

cd
mv IdeaIC* ".$(echo IdeaIC*)"
cd ~/.IdeaIC*/config
mkdir -p plugins
cd plugins
unzip /tmp/python-ce.zip
unzip /tmp/scala-intellij-bin-${scala_plugin_version}.zip

cd $HOME

mkdir -p $HOME/.ssh
# fingerprints for gitlab.com
cat > $HOME/.ssh/known_hosts << EOF
|1|i+VI1bhHR/jdiCygDAOcH5N9zRI=|V7XcevdJ+VRSaYdotyRWejuPoHA= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=
|1|Cm2HAispbdM/Zc9Yad5swtEDBSw=|v+vqL7+kH8S0/5safHTVzPWjdUQ= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=
EOF

if ! [[ -d $repo ]]; then
  git clone git@gitlab.com:scling-public/${repo}.git
fi

cd $repo

mv ~/idea .idea

git config user.name scling_course
git config user.email courses@scling.com

cd workflow

sudo -H pip3 install -r requirements.txt

cd ..

# bash

