package com.scling.exercises.simple

import com.scling.exercises.Order
import com.scling.exercises.Serde._
import com.scling.tinybigdata.Coll

// Read a dataset with orders. Some orders are duplicates, and have identical id fields. Remove duplicates and ensure
// that each order has a unique id.

object DeduplicateJob {
  def main(args: Array[String]): Unit = {
    val inputFile = args(1)
    val outputFile = args(2)

    val input = Coll.readJson[Order](inputFile)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[Order] = Coll.empty[Order]
    result.writeJson(outputFile)
  }
}
