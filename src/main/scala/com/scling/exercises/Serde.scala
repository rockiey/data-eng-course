package com.scling.exercises

import com.github.nscala_time.time.Imports.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.ISODateTimeFormat
import play.api.libs.json._

import scala.util.Try

// Definitions for how classes are serialised as JSON.

object Serde {
  // Teach Play JSON to parse dates with time zone. Inspired by play-json-date.
  implicit val dateTimeReads: Reads[DateTime] = new Reads[DateTime] {
    def reads(json: JsValue): JsResult[DateTime] = {
      val result = for {
        str <- json match {
          case JsString(s) => Some(s)
          case _ => None
        }
        date <- parseDate(str).toOption
      } yield date

      result.map(
        d => JsSuccess(d)
      ).getOrElse(
        JsError(Seq(JsPath() -> Seq(JsonValidationError("error.expected.date"))))
      )
    }

    private def parseDate(input: String): Try[DateTime] =
      Try {DateTime.parse(input, ISODateTimeFormat.dateOptionalTimeParser).withZone(DateTimeZone.UTC)}
  }

  implicit val dateTimeWrites: Writes[DateTime] = JodaWrites.JodaDateTimeWrites

  implicit val bigramCountReads: Reads[BigramCount] = Json.reads[BigramCount]
  implicit val bigramCountWrites: Writes[BigramCount] = Json.writes[BigramCount]
  implicit val pageViewReads: Reads[PageView] = Json.reads[PageView]
  implicit val pageViewWrites: Writes[PageView] = Json.writes[PageView]
  implicit val countryCountReads: Reads[CountryCount] = Json.reads[CountryCount]
  implicit val countryCountWrites: Writes[CountryCount] = Json.writes[CountryCount]
  implicit val itemCountReads: Reads[ItemCount] = Json.reads[ItemCount]
  implicit val itemCountWrites: Writes[ItemCount] = Json.writes[ItemCount]
  implicit val messagePostReads: Reads[MessagePost] = Json.reads[MessagePost]
  implicit val messagePostWrites: Writes[MessagePost] = Json.writes[MessagePost]
  implicit val orderReads: Reads[Order] = Json.reads[Order]
  implicit val orderWrites: Writes[Order] = Json.writes[Order]
  implicit val orderCountReads: Reads[OrderAggregate] = Json.reads[OrderAggregate]
  implicit val orderCountWrites: Writes[OrderAggregate] = Json.writes[OrderAggregate]
  implicit val pageRenderReads: Reads[PageRender] = Json.reads[PageRender]
  implicit val pageRenderWrites: Writes[PageRender] = Json.writes[PageRender]
  implicit val renderPercentilesReads: Reads[RenderPercentiles] = Json.reads[RenderPercentiles]
  implicit val renderPercentilesWrites: Writes[RenderPercentiles] = Json.writes[RenderPercentiles]
  implicit val recommendationReads: Reads[Recommendation] = Json.reads[Recommendation]
  implicit val recommendationWrites: Writes[Recommendation] = Json.writes[Recommendation]
  implicit val sessionReads: Reads[Session] = Json.reads[Session]
  implicit val sessionWrites: Writes[Session] = Json.writes[Session]
  implicit val trendingItemReads: Reads[TrendingItem] = Json.reads[TrendingItem]
  implicit val trendingItemWrites: Writes[TrendingItem] = Json.writes[TrendingItem]
  implicit val userReads: Reads[User] = Json.reads[User]
  implicit val userWrites: Writes[User] = Json.writes[User]
  implicit val userLastActiveReads: Reads[UserLastActive] = Json.reads[UserLastActive]
  implicit val userLastActiveWrites: Writes[UserLastActive] = Json.writes[UserLastActive]
  implicit val viewTimePercentReads: Reads[ViewTimePercent] = Json.reads[ViewTimePercent]
  implicit val viewTimePercentWrites: Writes[ViewTimePercent] = Json.writes[ViewTimePercent]
  implicit val viewTimeDeclineReads: Reads[ViewTimeDecline] = Json.reads[ViewTimeDecline]
  implicit val viewTimeDeclineWrites: Writes[ViewTimeDecline] = Json.writes[ViewTimeDecline]
}

