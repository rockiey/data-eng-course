package com.scling.tinybigdata

import scala.math.Ordering

class KColl[K, T](private val groups: Map[K, List[T]]) extends Coll[(K, T)](List.empty[(K, T)]) {
  override def collect(): List[(K, T)] = groups.toList.flatMap(kv => kv._2.map(v => (kv._1, v)))

  def count: KColl[K, Long] = eachGroup(vals => List(vals.length.toLong))

  def fold[U](zero: => U)(foldFn: (U, T) => U): KColl[K, U] =
    eachGroup(vals => List(vals.foldLeft(zero)(foldFn)))

  def foldWithKey[U](zero: K => U)(foldFn: (U, T) => U): KColl[K, U] =
    eachKeyGroup(key => { vals => List(vals.foldLeft({zero(key)})(foldFn)) })

  def reduce(reduceFn: (T, T) => T): KColl[K, T] =
    eachGroup(vals => List(vals.reduce(reduceFn)))

  def join[U](right: KColl[K, U]): KColl[K, (T, U)] =
    doJoin[U, U](right, { (lVal, rVals) => rVals.map(rv => (lVal, rv)) })

  def leftJoin[U](right: KColl[K, U]): KColl[K, (T, Option[U])] =
    doJoin[U, Option[U]](right,
      { (lVal, rVals) =>
        if (rVals.isEmpty) {
          val n: Option[U] = None
          List((lVal, n))
        }
        else
          rVals.map(rv => (lVal, Some(rv)))
      })

  def doJoin[U, UO](right: KColl[K, U], combiner: (T, List[U]) => List[(T, UO)]): KColl[K, (T, UO)] =
    new KColl[K, (T, UO)](
      groups.flatMap({ keyVals =>
        val leftKey = keyVals._1
        val leftValues = keyVals._2
        val combined = leftValues.flatMap(lv => {
          val rightValues = right.groups.getOrElse(leftKey, List())
          combiner(lv, rightValues)
        })
        if (combined.nonEmpty)
          Some((leftKey, combined))
        else
          None
      }))

  override def log(label: String, printer: String => Unit = System.err.println): KColl[K, T] = {
    super.log(label, printer)
    this
  }

  def sortBy[U](keyFn: T => U)(implicit ord: Ordering[U]): KColl[K, T] =
    eachGroup(_.sortBy(keyFn))

  def take(n: Int): KColl[K, T] = eachGroup(_.take(n))

  def values: Coll[T] = map(_._2)

  private def eachKeyGroup[U](fn: K => List[T] => List[U]): KColl[K, U] = {
    KColl.from(groups.map({ case (key, vals) => (key, fn(key)(vals)) }))
  }

  private def eachGroup[U](fn: List[T] => List[U]): KColl[K, U] = {
    KColl.from(groups.mapValues(fn))
  }

  override def toString: String = {
    val keyStrings = groups.map(
      keyVal => keyVal._1.toString + " -> " + keyVal._2.mkString("[\n    ", ",\n    ", "\n  ]")
    )
    keyStrings.mkString("{\n  ", ",\n  ", "}")
  }
}

object KColl {
  def from[K, T](keyMap: Map[K, List[T]]): KColl[K, T] = new KColl[K, T](keyMap)
}
