package com.scling.exercises.complex

import better.files.File
import com.github.nscala_time.time.Imports.{DateTime, _}
import com.scling.exercises._
import org.scalatest.FunSuite

class CollaborativeFilterJobTest extends FunSuite with BatchJobTestRunner1[PageView, Recommendation] {

  import Serde._

  implicit val recommendationOrdering: Ordering[Recommendation] = Ordering.by(Recommendation.unapply)

  override def runJob(pageView: File, output: File): Unit =
    CollaborativeFilterJob.main(Array("ViewTimeDeclineJob", pageView.toString, output.toString))

  val one = "http://www.movies.com/episode_one"
  val two = "http://www.movies.com/episode_two"
  val three = "http://www.movies.com/episode_three"
  val four = "http://www.movies.com/episode_four"
  val five = "http://www.movies.com/episode_five"
  val six = "http://www.movies.com/episode_six"

  def ten: DateTime = DateTime.parse("2018-01-01T10:00:00Z")

  ignore("Five users") {
    assert(runTest(Seq(
      PageView("alice", one, ten),
      PageView("alice", two, ten + 3.hours),
      PageView("alice", three, ten + 6.hours),
      PageView("alice", one, ten + 9.hours),
      PageView("bob", one, ten),
      PageView("bob", two, ten + 3.hours),
      PageView("bob", six, ten + 6.hours),
      PageView("cecilia", two, ten),
      PageView("cecilia", four, ten + 3.hours),
      PageView("cecilia", five, ten + 6.hours),
      PageView("david", four, ten + 6.hours),
      PageView("david", five, ten + 6.hours),
      PageView("david", six, ten + 6.hours)
    )) ===
      Seq(Recommendation("alice", six),
        Recommendation("bob", three),
        Recommendation("cecilia", six),
        Recommendation("david", two)))
  }
}
