package com.scling.exercises.simple

import better.files.File
import com.scling.exercises.{BatchJobTestRunner1, ItemCount, Order}
import org.scalatest.FunSuite
import com.scling.exercises.TestInput
import com.scling.exercises.Serde

class TopItemsJobTest extends FunSuite with BatchJobTestRunner1[Order, ItemCount] {

  import Serde._
  import TestInput._

  implicit val itemCountOrdering: Ordering[ItemCount] = Ordering.by(ItemCount.unapply)

  def runJob(input: File, output: File): Unit =
    TopItemsJob.main(Array("ItemCountsJob", "3", input.toString, output.toString))

  test("Empty input") {
    assert(runTest(Seq()) === Seq())
  }

  ignore("Single order") {
    val input = Seq(Order("1", "alice", "pants"))
    assert(runTest(input) === Seq(ItemCount("pants", 1)))
  }

  ignore("Two items") {
    val input =
      orders(1, "alice", "pants") ++
        orders(2, "bob", "pants") ++
        orders(1, "david", "shirt")

    assert(runTest(input) === Seq(
      ItemCount("pants", 3),
      ItemCount("shirt", 1)
    ))
  }

  ignore("Only top items") {
    val input =
      orders(6, "bob", "beanie") ++
        orders(6, "alice", "pants") ++
        orders(2, "cecilia", "skirt") ++
        orders(1, "bob", "trousers") ++
        orders(1, "david", "shirt")

    assert(runTest(input) === Seq(
      ItemCount("beanie", 6),
      ItemCount("pants", 6),
      ItemCount("skirt", 2)
    ))
  }
}
