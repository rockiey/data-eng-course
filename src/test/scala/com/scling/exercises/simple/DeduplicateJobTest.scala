package com.scling.exercises.simple

import better.files.File
import com.scling.exercises.{BatchJobTestRunner1, Order, Serde}
import org.scalatest.FunSuite


class DeduplicateJobTest extends FunSuite with BatchJobTestRunner1[Order, Order] {

  import Serde._

  implicit val orderOrdering: Ordering[Order] = Ordering.by(Order.unapply)

  override def runJob(inputPath: File, outputPath: File): Unit =
    DeduplicateJob.main(Array("DeduplicateJob", inputPath.toString, outputPath.toString))

  test("Empty input") {
    assert(runTest(Seq()) === Seq())
  }

  ignore("Single item") {
    val input = Seq(Order("1", "alice", "pants"))
    assert(runTest(input) === input)
  }

  ignore("Distinct items") {
    val input = Seq(Order("1", "alice", "pants"), Order("2", "bob", "shirt"))
    assert(runTest(input) === input)
  }

  ignore("One duplicate") {
    val input = Seq(Order("1", "alice", "pants"), Order("1", "alice", "pants"), Order("2", "bob", "shirt"))
    assert(runTest(input) === input.tail)
  }

  ignore("Duplicate, different fields") {
    val input = Seq(Order("1", "alice", "pants"), Order("1", "Alice again", "pants"), Order("2", "bob", "shirt"))
    assert(runTest(input).map(_.id) === Seq("1", "2"))
  }

  ignore("Two duplicates") {
    val input = Seq(
      Order("1", "alice", "pants"),
      Order("1", "alice", "pants"),
      Order("2", "bob", "shirt"),
      Order("3", "cecilia", "skirt"),
      Order("2", "bob", "shirt"))
    assert(runTest(input) === input.tail.dropRight(1))
  }

  ignore("Double duplicate") {
    val input = Seq(
      Order("1", "alice", "pants"),
      Order("2", "bob", "shirt"),
      Order("3", "cecilia", "skirt"),
      Order("2", "bob", "shirt"),
      Order("2", "bob", "shirt"))
    assert(runTest(input) === input.dropRight(2))
  }
}
