#! /usr/bin/env python3

import argparse
import sys
from pathlib import Path

from lib.dataset import read_json_lines, write_json_lines


"""Aggregate multiple datasets through concatenation."""

def main(argv):
    parser = argparse.ArgumentParser()
    # Accept the superset of arguments used in the exercises.
    parser.add_argument('--inputs')
    parser.add_argument('--output')
    args = parser.parse_args(argv)

    aggregate = []
    for in_file in args.inputs.split(','):
        aggregate.extend(read_json_lines(Path(in_file)))

    write_json_lines(Path(args.output), aggregate)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
