#! /usr/bin/env python3

import argparse
import sys
from datetime import datetime, timedelta
from pathlib import Path

from lib.dataset import read_json_lines, write_json_lines, dict_group_by


"""Aggregate multiple datasets through concatenation. Filter out datasets not matching a particular hour."""

def pick_if_match(records, hour):
    recs = list(records)
    if recs:
        s_rec = sorted(recs, key=lambda r: r['time'])
        t = datetime.strptime(s_rec[0]['time'], '%Y-%m-%dT%H:%M:%S.%fZ')
        if hour <= t < (hour + timedelta(hours=1)):
            return [s_rec[0]]
    return []


def main(argv):
    parser = argparse.ArgumentParser()
    # Accept the superset of arguments used in the exercises.
    parser.add_argument('--hour')
    parser.add_argument('--inputs')
    parser.add_argument('--output')
    args = parser.parse_args(argv)

    hour = datetime.strptime(args.hour, '%Y-%m-%d %H:%M:%S')

    aggregate = []
    for in_file in args.inputs.split(','):
        aggregate.extend(read_json_lines(Path(in_file)))

    out = []
    for _, group in dict_group_by(aggregate, 'id'):
        out.extend(pick_if_match(group, hour))

    write_json_lines(Path(args.output), out)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
