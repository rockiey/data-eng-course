# noinspection PyUnresolvedReferences
import json
from pathlib import Path
# noinspection PyUnresolvedReferences
from unittest import TestCase, skip

from lib.coordinates import TEST_LAKE
from test.harness import run_luigi, write_dataset, read_dataset, WorkflowTestCase


class LambdaTest(WorkflowTestCase):
    def run_job(self):
        run_luigi('exercises.complex.lambda', 'RangeHourlyBase', of='PageviewCleanFastAndSlow',
                  start='2019-10-13T00', stop='2019-10-14T00', hours_back=24 * 365 * 10)

    def _cold_path(self, h):
        return Path(f'{TEST_LAKE}/cold/PageviewHourly/year=2019/month=10/day=13/hour={h:02}')

    def _clean_path(self, w, h):
        return Path(f'{TEST_LAKE}/derived/PageviewCleanHourly/window={w}/year=2019/month=10/day=13/hour={h:02}')

    @skip("Remove this line to enable the test")
    def test_happy(self):
        cold_08 = [{'url': 'https://www.acme.com/', 'time': '2019-10-13T08:01:02.492743Z',
                    'id': '123e4567-e89b-12d3-a456-426655440000'}]
        write_dataset(self._cold_path(8), cold_08)

        self.run_job()

        clean_08 = read_dataset(self._clean_path(1, 8))
        self.assertCountEqual(clean_08, cold_08)
        self.assertIsNone(read_dataset(self._clean_path(4, 8)))

        cold_09 = [{'url': 'https://www.acme.com/', 'time': '2019-10-13T08:01:02.492999Z',
                    'id': '123e4567-e89b-12d3-a456-426655440000'},
                   {'url': 'https://www.acme.com/about/9', 'time': '2019-10-13T09:02:03.492743Z',
                    'id': 'ff3e4567-e89b-12d3-a456-426655440000'}]
        write_dataset(self._cold_path(9), cold_09)

        self.run_job()
        clean_09 = read_dataset(self._clean_path(1, 9))
        self.assertCountEqual(clean_09, cold_09[1:])
        self.assertIsNone(read_dataset(self._clean_path(4, 8)))
        self.assertIsNone(read_dataset(self._clean_path(4, 9)))

        cold_10 = [{'url': 'https://www.acme.com/10', 'time': '2019-10-13T10:01:02.492999Z',
                    'id': '103e4567-e89b-12d3-a456-426655440000'},
                   {'url': 'https://www.acme.com/about/10', 'time': '2019-10-13T08:02:03.492743Z',
                    'id': '813e4567-e89b-12d3-a456-426655440000'},
                   {'url': 'https://www.acme.com/about/10', 'time': '2019-10-13T10:02:03.492743Z',
                    'id': 'aa3e4567-e89b-12d3-a456-426655440000'}]
        write_dataset(self._cold_path(10), cold_10)

        cold_11 = [{'url': 'https://www.acme.com/late/8', 'time': '2019-10-13T08:11:02.492999Z',
                    'id': '183e4567-e89b-12d3-a456-426655440000'},
                   {'url': 'https://www.acme.com/about/11', 'time': '2019-10-13T11:02:03.492743Z',
                    'id': '113e4567-e89b-12d3-a456-426655440000'}]
        write_dataset(self._cold_path(11), cold_11)

        self.run_job()

        clean_11 = read_dataset(self._clean_path(1, 11))
        self.assertCountEqual(clean_11, cold_11[1:])

        # We have now gathered input to 08 hours from four datasets, and the slow variant should be ready.
        slow_8 = read_dataset(self._clean_path(4, 8))
        self.assertCountEqual(slow_8, cold_08 + cold_10[1:2] + cold_11[:1])

        self.assertIsNone(read_dataset(self._clean_path(4, 9)))
        self.assertIsNone(read_dataset(self._clean_path(4, 10)))
        self.assertIsNone(read_dataset(self._clean_path(4, 11)))
