# Scling workflow orchestration exercises

## Getting started

First, you need to set up your environment as per [the instructions in the root
folder](../README.md).

Each workflow exercise is an unfinished workflow DAG, which you are supposed to implement. The
workflow files are located under `workflow/exercises`. Each workflow file has a corresponding file
with test cases, and your task is to make the test cases pass. The test cases, located in
`workflow/test/*/test_*.py`, typically populate a test data lake with a few datasets, runs your
workflow, which in turn may run the simple programs provided in `workflow/bin` to perform data
processing.

When you decide to pick up a particular exercise, open the corresponding test case, and remove the
lines starting with `@skip`. Edit the workflow file matching the test case name until the tests pass.

## Running the tests ##

You have different options for running the tests. IntelliJ is most complex to set up, but provides
the best support for type assistance, quick iteration, and debugging. If you cannot get it to work,
you may opt for one of the other options.

### Preparing IntelliJ ###

If you are using the VirtualBox or remote desktop lab setup, IntelliJ should already be configured
for you, and you can skip to the next section. The steps in this section are only necessary for
students using the native setup, or if you run into trouble with IntelliJ.

You will need to configure IntelliJ to use the appropriate Python interpreter. If you created a
virtual environment, you will need to inform IntelliJ with the steps below. Note that it might be
best to also have your virtual environment in PATH, i.e. by executing `source
venv/bin/activate` before starting IntelliJ. In order for this to have appropriate impact,
you will need to launch IntelliJ from the command line (`idea.sh` in Linux).

Configuring your virtual environment in IntelliJ:

* IntelliJ: File -> Project Structure -> SDKs -> Add New SDK (+ symbol) -> Python SDK -> Virtualenv
  environment -> Existing environment. 
* Select the file `venv/bin/python3`.

If you have set up IntelliJ for the batch processing exercises, there should be a module called
`workflow`, which should have the `Python` facet. Verify this in the project structure:

* IntelliJ: File -> Project Structure -> Facets.
* You should find `Python (workflow)` in the list. If it is absent, add it:
* IntelliJ: File -> Project Structure -> Facets -> Add (+ symbol) -> `workflow`.
* Click `Apply`.

Verify that we are using the right Python interpreter (the venv SDK you created above).

* IntelliJ: File -> Project Structure -> Modules -> `<repository_name>/workflow/workflow` ->
  Dependencies -> Module SDK. Select the venv Python SDK.
* IntelliJ: File -> Project Structure -> Modules -> `<repository_name>/workflow/workflow/Python` ->
  Python interpreter. Select the venv Python SDK. 
  
Phew, lots of hoops with IntelliJ. In some cases, IntelliJ defaults to the right settings, but not
always. If you get issues with the Python setup, revisit these instructions. If you prefer PyCharm,
the instructions are more or less the same as for IntelliJ.

### Testing and debugging with IntelliJ ###

This is the primary method for running the tests.

You should now be able to run workflow tests inside IntelliJ. Open the file
`workflow/test/demo/test_demographic_sales.py`, but the cursor on the class `DemographicSalesTest`,
right click, and select `Run Unittests for test_demographic_sales.DemographicSalesTest`. Note `Run
Unittests for ..Test.py`, not `Run ..Test.py`. If that option does not show up, something is
probably wrong with your IntelliJ setup, and it cannot deduce that it is a unittest test case.

### Python unittest framework, Gradle ###

This is a secondary method.

In order to run the tests with Python unittest, go to the `workflow` directory and type:

* `PYTHONPATH=. python3 -m unittest discover`

Note that if you use a virtual environment, you have to have the virtual environment activated
(`source venv/bin/activate`).

In order to edit the exercise files, use any text editor you like, Emacs, Sublime, etc. Don't forget
to save before you run a test. :-)

You can run the Python tests via Gradle if you want:

* `cd .. && ./gradlew :workflow:pytest`

It is merely an alias for `python3 -m unittest discover`. The Gradle target `testAll` will run both
Scala and Python tests, verifying your full setup.


### Inside Docker ###

This is a backup method, not tested regularly. 

In case you cannot get your environment to run the exercises, you can run the tests inside
Docker. You can still edit source files with your preferred editor, and changes will be picked up
inside the Docker container. First, remove any virtual environment you might have created, then
follow the Docker instructions in [../README.md](../README.md). Summary below:

* `rm -rf venv`
* `cd ..`
* `./docker_gradle.sh`

You should now be inside a Docker container, and a virtual environment should be activated.

Windows users:
* `fromdos win_docker_test.sh`
* `./win_docker_test.sh :workflow:pytest`

Linux, MacOS users:
* `./gradlew :workflow:pytest`

A couple of the workflow tests run Docker in the test harness, and these may fail with this
method. 
