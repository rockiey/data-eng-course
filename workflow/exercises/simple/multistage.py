import logging
from datetime import timedelta

from luigi import DateHourParameter
from luigi import ExternalTask
from luigi.contrib.external_program import ExternalProgramTask
from luigi.target import FileSystemTarget
from luigi.task import Task, externalize

from lib.coordinates import LAKE
from lib.local_target import LocalFlagTarget

# In this exercise, we build a two-stage pipeline, where the first step copies raw source files from an ingest location
# to the cold pond. The ingestion step would in production be explicitly scheduled. After ingestion, we run a
# deduplication step, which is scheduled as a separate task. The Luigi concepts ExternalTask and externalize are your
# friends.


# Helper class.
class CopyFlagDatasetTask(Task):
    """Copy a *FlagTarget dataset from one location to another.

    Ensure that the flag file is copied last. It guarantees consistent consumption on file systems
    that guarantee total store order. HDFS, GCS, NFS do so, but S3 does not.

    Derive from this class and override requires() and output().
    """

    def run(self):
        if not isinstance(self.input(), FileSystemTarget) or not hasattr(self.input(), 'flag'):
            raise ValueError(f'CopyFlagDatasetTask expects a single *FlagTarget as input, got {self.input()}')
        if not isinstance(self.output(), FileSystemTarget) or not hasattr(self.output(), 'flag'):
            raise ValueError(f'CopyFlagDatasetTask expects a single *FlagTarget as output, got {self.output()}')
        fs = self.output().fs
        for src_file in fs.listdir(self.input().path):
            src_base = src_file.rsplit('/', 1)[-1]
            # Must copy flag file last for consistency.
            if src_base not in (self.input().flag, self.output().flag):
                self._copy(fs, self.input().path + src_base, self.output().path + src_base)
        src_flag = self.input().path + self.input().flag
        dst_flag = self.output().path + self.output().flag
        self._copy(fs, src_flag, dst_flag)

    def _copy(self, fs, src, dst):
        logging.debug(f'Copying {src} to {dst}')
        fs.copy(src, dst)


class PageviewDedup(ExternalProgramTask):
    """Read 2 hourly datasets. Emit items that are not duplicates, and are timestamped in the hour we care about."""

    # Insert solution code here. Most Luigi tasks need one or more of:
    # job parameters (date, etc), requires() method, and output() or complete() method,
    # as well as methods specific to type of task, e.g. program_args (ExternalProgramTask), database (CopyToTable), ...


